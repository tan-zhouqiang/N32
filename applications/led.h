/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-14     86135       the first version
 */
#ifndef APPLICATIONS_LED_H_
#define APPLICATIONS_LED_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#define LED1_PIN    57

void led_init();
void led(int a);

#endif /* APPLICATIONS_LED_H_ */
