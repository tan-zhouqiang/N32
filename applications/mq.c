/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-14     86135       the first version
 */
#include "mq.h"
#include "usart.h"

rt_mq_t temp_mq = RT_NULL;

static void thread_entry(void *parameter)
{
  (void)parameter;
  rt_err_t result = RT_EOK;
  char tempBuf[6] ={0};
  while(1)
  {
    result = rt_mq_recv(temp_mq,                                // 消息队列对象句柄
                        tempBuf,                                // 消息
                        sizeof(tempBuf),                        // 消息大小
                        1000);                                   // 一直等待
    if(result == RT_EOK)
    {
        rt_kprintf("thread2: recv message number: %s\n", tempBuf);// 打印接收到的消息序号
        rt_device_write(serial, 0, tempBuf, 6);
    }
    rt_thread_mdelay(1500);                                      // 延时550ms
  }
}


int mq_sample_init(void)
{
    rt_err_t result = RT_EOK;
    rt_thread_t thread = RT_NULL;
    temp_mq = rt_mq_create("temp",                                // 消息队列名称
                           32,                                    // 一条消息最大长度，单位：字节
                           5,                                     // 消息最大数量
                           RT_IPC_FLAG_FIFO);                     // 先进先出模式
    if(temp_mq == RT_NULL)
       return -1;
    thread = rt_thread_create("thread1",                         // 线程名称
                                 thread_entry,                     // 线程入口函数
                                 RT_NULL,                           // 入口函数传入参数
                                 1024,                               // 线程堆栈大小
                                 12,                                 // 线程优先级
                                 20);                               // 时间片
      if(thread == RT_NULL)
        return -1;
      result = rt_thread_startup(thread);                          // 启动线程
      return result;                                                // 返回创建及启动结果
}
