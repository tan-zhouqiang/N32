/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-14     86135       the first version
 */
#include "led.h"

void led_init()
{
    rt_pin_mode(LED1_PIN, PIN_MODE_OUTPUT);
}

void led(int a)
{
   rt_pin_write(LED1_PIN, a);
}
