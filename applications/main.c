#include <stdint.h>
#include <rtthread.h>
#include <rtdevice.h>
#include "usart.h"
#include "ht11.h"
#include "mq.h"

int main(void)
{
    uart_sample();
    dht11_thread_init();
    mq_sample_init();
}

