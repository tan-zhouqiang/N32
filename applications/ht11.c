#include "ht11.h"
#include "mq.h"

#define DATA_PIN          PKG_USING_DHTXX_SAMPLE_PIN

rt_thread_t dht11_thread = RT_NULL;
char str[6];

static void dht11_thread_entry(void *parameter)
{
    (void)parameter;
    dht_device_t sensor;
    rt_int32_t humi;
    rt_int32_t temp;
    while(1){
        rt_thread_mdelay(1500);
        sensor = dht_create(DATA_PIN);
        if(dht_read(sensor)) {

           temp = dht_get_temperature(sensor)/10;
           humi = dht_get_humidity(sensor)/10;
           str[0]='#';
           str[1]=(temp/10%10)+'0';
           str[2]=(temp%10)+'0';
           str[3]='#';
           str[4]=(humi/10%10)+'0';
           str[5]=(humi%10)+'0';
           //rt_kprintf("%s\n", str);
           rt_mq_send(temp_mq,str,sizeof(str));
        }
        dht_delete(sensor);
    }
}

int dht11_thread_init(void)
{
    dht11_thread = rt_thread_create("dht11", dht11_thread_entry, RT_NULL, 1024, 11, 20);
    if(dht11_thread == RT_NULL) return -1;
    return rt_thread_startup(dht11_thread);
}
