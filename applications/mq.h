/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-14     86135       the first version
 */
#ifndef APPLICATIONS_MQ_H_
#define APPLICATIONS_MQ_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>


extern rt_mq_t temp_mq;
int mq_sample_init(void);

#endif /* APPLICATIONS_MQ_H_ */
