/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-02-22     86135       the first version
 */
#ifndef APPLICATIONS_USART_H_
#define APPLICATIONS_USART_H_

#include <rtthread.h>
#define SAMPLE_UART_NAME       "uart2"      /* 串口设备名称 */
/* 用于接收消息的信号量 */
struct rt_semaphore rx_sem;

extern  rt_device_t serial;

static rt_err_t uart_input(rt_device_t dev, rt_size_t size);
static void serial_thread_entry(void *parameter);

int uart_sample();

#endif /* APPLICATIONS_USART_H_ */
