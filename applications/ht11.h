/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-03-04     86135       the first version
 */
#ifndef APPLICATIONS_HT11_H_
#define APPLICATIONS_HT11_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "dhtxx.h"
#include "usart.h"

int dht11_thread_init(void);

#endif /* APPLICATIONS_HT11_H_ */
